FROM debian:bookworm-slim

RUN set -eux; \
	apt update; \
	apt install -y --no-install-recommends \
		ca-certificates \
		iptables \
		openssl \
		pigz \
		xz-utils \
		curl \
		python3 \
		python3-pip \
		make \
	; \
	rm -rf /var/lib/apt/lists/*

ENV DOCKER_TLS_CERTDIR=/certs

RUN mkdir /certs /certs/client && chmod 1777 /certs /certs/client

COPY --from=docker:25.0.4-dind /usr/local/bin/ /usr/local/bin/
COPY --from=docker:25.0.4-cli /usr/local/libexec/docker/cli-plugins /usr/local/libexec/docker/cli-plugins

VOLUME /var/lib/docker
VOLUME /var/run/docker.sock /var/run/docker.sock

EXPOSE 2375 2376

ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []
